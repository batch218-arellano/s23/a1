// console.log ("Hello World!");


let trainer = {
    name : 'Ash Ketchum',
    age : 10,
    pokemon : ['Pikachu', 'Charizard', 'Squirtle', 'Bulbasaur'],
    friends : { kanto : ['Brock','Misty'], hoenn: ['May', 'Max']},
    talk:function(){
        console.log(this.name);
    }
}; 
console.log(trainer);
console.log("Result of dot notation: ");
trainer.talk();

console.log("Result of square bracket notation: ");
console.log(trainer['pokemon']);

let pokemon = {
    name: 'Pikachu',
    talk: function(){
        console.log(this.name + "!" + "I choose you!");
    }
}
console.log("Result of talk method");
pokemon.talk();

function Pokemon(name, level){

    //Properties
    this.name = name;
    this.level = level;
    this.health = level * 2;
    this.attack = level;

    this.tackle = function(target){
        console.log(this.name + " tackled " + target.name);
        
        target.health -= this.attack
        console.log(target.name + "'s" +  " health is now reduceed to "+target.health);

        if(target.health <= 0){
            target.faint();
        }
    }
    this.faint = function(){
        console.log(this.name + " fainted ");
    }
}

let pikachu = new Pokemon("Pikachu", 12);
console.log(pikachu);

let geodude = new Pokemon("Geodude", 8);
console.log(geodude);

let mewtwo = new Pokemon("Mewtwo", 100);
console.log(mewtwo);
 
geodude.tackle(pikachu); 

console.log(pikachu);

mewtwo.tackle(geodude); 

console.log(geodude);